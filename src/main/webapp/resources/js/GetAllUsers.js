/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

// DO GET
function ajaxGet(){
    $.ajax({
        type : "GET",
        url : window.location + "/users",
        dataType: 'json',
        success: function(result){
            $('#user_info').empty();
            var userList = "";

            if (result.length == 0) {
                $(".alert-danger").show();
                toggleAlert("#no-user-alert");
                return;
            }
            $.each(result, function(i, user){
                var id = user.id;
                var $tr = $('<tr>').append(
                    $('<td>').text(user.id),
                    $('<td>').text(user.name),
                    $('<td>').text(user.age),
                    $('<td>').text(user.address),
                    $('<td>').text(user.user_name),
                    $('<td>').text(user.password),
                    $('<td>').innerHTML = '<button class="btn btn-primary" onclick="edit('+id+')" )">Edit</button>\n' +
                        '<button class="btn btn-danger" onclick="ajaxDelete('+id+')">Delete</button>\n' );
                $('#getResultDiv #user_info').append($tr);
            });
            console.log("Success: ", result);
        },
        error : function(e) {
            $("#getResultDiv").html("<strong>Error</strong>");
            console.log("ERROR: ", e);
        }
    });
}

function edit(id) {
    window.location = "/update/?id=" + id;
}

$( document ).ready(function() {

    $('#user_info').empty();
    ajaxGet();

    // GET REQUEST
    $("#btn_all").click(function(event){
        event.preventDefault();
        ajaxGet();
        $('#getResultDiv').toggle();

        if ($.trim($(this).text()) === 'Show Users') {
            $(this).text('Hide Users');
        } else {
            $(this).text('Show Users');
        }

    });
})