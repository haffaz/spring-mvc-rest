/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

$(document).ready(function () {

    // SUBMIT FORM
    $("#userForm").submit(function (event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();
        ajaxCreate();
    });


    function ajaxCreate() {
        // PREPARE FORM DATA
        var formData = {
            name: $("#input_name").val(),
            age: $("#input_age").val(),
            address: $("#input_address").val(),
            user_name: $("#input_user_name").val(),
            password: $("#input_password").val()
        }

        // DO POST
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: window.location + "/user/create",
            data: JSON.stringify(formData),
            dataType: 'json',
            success: function (result) {
                //alert("Successfully added user " + result.name);
                $(".alert-success").show();
                toggleAlert("#success-register");
                console.log(result);
            },
            error: function (e) {
                $('.alert-danger').show();
                toggleAlert("#error-register");
                console.log("ERROR: ", e);
            }
        });
        // Reset FormData after Posting
        resetData();
    }

    function resetData() {
        $("#input_name").val("");
        $("#input_age").val("");
        $("#input_address").val("");
        $("#input_user_name").val("");
        $("#input_password").val("");
    }
});