/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

/**
 * Gets the selected users data and calls
 * the populate method
 * @param id
 */
function loadUpdateForm(id) {

    $.ajax({
        type: "GET",
        url: "/user/" + id,
        dataType: 'json',
        success: function (result) {
            populateForm(result);
            console.log("Success: ", result);
        },
        error: function (e) {
            $("#getResultDiv").html("<strong>Error</strong>");
            console.log("ERROR: ", e);
        }
    });


}

function UpdateUser(id) {
    // updated user data as json
    var formData = {
        name: $("#input_name").val(),
        age: $("#input_age").val(),
        address: $("#input_address").val(),
        user_name: $("#input_user_name").val(),
        password: $("#input_password").val()
    }

    // PUT request to update user data
    $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "/user/edit/" + id,
        data: JSON.stringify(formData),
        success: function (result) {
            $(".alert").show();
            toggleAlert();
            console.log("SUCCESS: ", result);
        },
        error: function (e) {
            console.log("ERROR: ", e);
        }
    });
    resetData();
}

/**
 * Populates form fields with selected users current
 * information
 * @param user : selected user
 */
function populateForm(user) {
    $("#input_name").val(user.name);
    $("#input_age").val(user.age);
    $("#input_address").val(user.address);
    $("#input_user_name").val(user.user_name);
    $("#input_password").val(user.password);
}

function toggleAlert() {
    window.setTimeout(function () {
        $("#success-register").fadeTo(1000, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
}

function resetData() {
    $("#input_name").val("");
    $("#input_age").val("");
    $("#input_address").val("");
    $("#input_user_name").val("");
    $("#input_password").val("");
}

$(document).ready(function () {

    //parameter passed to the url
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var id = getUrlParameter('id');
    loadUpdateForm(id);

    // SUBMIT FORM
    $("#userForm").submit(function (event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();
        UpdateUser(id);
    });
});